import moment from 'moment';

export const constructDateObject = (date) => ({
    timeStamp: date.valueOf(),
    displayDate: date.format('DD MMMM YYYY'),
})