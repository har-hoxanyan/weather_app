var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './index.js',
  output: { path: __dirname, filename: 'bundle.js' },
    module: {
        loaders: [
            {
                test: /.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015'],
                }
            }
        ]
    },
    resolve: {
        alias: {
            helpers: path.resolve(__dirname, 'helpers/'),
            utils: path.resolve(__dirname, 'utils/'),
            elements: path.resolve(__dirname, 'dom-elements/'),
        },
        extensions: ['.js']
    },
};