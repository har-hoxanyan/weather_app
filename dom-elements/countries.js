export const renderCountries = (domElement, countries) => {

    domElement.innerHTML = '';

    countries.forEach(country => {
        const countryItem = document.createElement('option');
        countryItem.innerHTML = country.name;
        countryItem.value = country.code;
        domElement.appendChild(countryItem);
    })


    domElement.onchange = function() {
        const event = new CustomEvent(
            'renderCities',
            {
                detail: window.cities.filter(item => item.country == this.value)
            }
        );
        document.dispatchEvent(event);
    }
}