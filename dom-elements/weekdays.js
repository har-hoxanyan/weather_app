import moment from 'moment';

import { constructDateObject } from 'utils/functions';

export const renderWeekdays = (rootElement) => {

    for (let i = 0; i < 5; i++) {
        const date = moment().add(i, 'day');
        const dateObj = constructDateObject(date);

        const dateItem = document.createElement('li');
        dateItem.innerHTML = dateObj.displayDate;
        dateItem.onclick = function() {
            console.log(dateObj, 'dateObj');
        }
        rootElement.appendChild(dateItem);
    }
}