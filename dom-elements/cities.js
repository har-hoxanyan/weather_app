import { getWeather } from '../api';

export const renderCities = (domElement, citiesList) => {

    domElement.innerHTML = '';

    citiesList.forEach(city => {
        const cityItem = document.createElement('option');
        cityItem.innerHTML = city.name;
        cityItem.value = city.id;
        domElement.appendChild(cityItem);
    });

    domElement.onchange = function() {
        getWeather(this.value).then(weathers => {
            window.weathers = weathers;

            const event = new CustomEvent('renderWeathers', {detail: weathers});
            document.dispatchEvent(event);
        });
        //(this.value);
    }
}