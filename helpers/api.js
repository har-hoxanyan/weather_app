const baseSettings = {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
};

function ApiError(message) {
    this.name = 'ApiError';
    this.ResultMessage = message;
    this.stack = (new Error()).stack;
}
ApiError.prototype = Object.create(Error.prototype);
ApiError.prototype.constructor = ApiError;

export default class Api {
    constructor(indexUrl) {
        this.indexUrl = indexUrl;
    }

    fetch(relative, m = 'GET', data = null, opt = null) {
        // UpperCase() Type and check for valid entry
        const method = m.toUpperCase();

        if ([ 'GET', 'POST', 'DELETE', 'PUT' ].indexOf(method) == -1) {
            throw new Error('Illegal method \'' + method + '\' for FETCH Request');
        }

        // Fix customOptions if null/invalid
        const requestOptions = Object.prototype.toString.call(opt) === '[object Object]' ? opt : {};

        // Construct Request URI
        const uri = this.indexUrl + relative;

        // Merge BaseSettings with onthefly-constructed RequestSettings

        let options = Object.assign({}, baseSettings, { method });

        // Merge Options again with supplied customOptions
        if (requestOptions) {
            options = Object.assign({}, options, requestOptions);
        }

        const contentTypes = options.headers && options.headers['Content-Type'];

        // check if we set content-type as json
        if (contentTypes && contentTypes.indexOf && contentTypes.indexOf('json') != -1) {
            if (options.body && (typeof options.body != 'string')) {
                options.body = JSON.stringify(options.body);
            } else if (data) {
                options.body = JSON.stringify(data);
            }
        } else {
            options.body = options.body || data || null;

        }

        // Fire the Request and Return the response promise Object

        const request = new Request(uri, options);

        return fetch(request).then(response => {
            if (response.ok) {
                return response;
            }
            response.json().then(r => {
                throw new ApiError(r.ExceptionMessage);
            });
        });
    }

    // We only need GET method
    // TODO declare other methods too

    /* GET */
    get(relative, options = null) {
        return this.fetch(relative, 'GET', null, options);
    }
}
