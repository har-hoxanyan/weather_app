import Api from 'helpers/api';
const apiKey = 'b28c046e7468308f9b3bf8853aef78d5';

export const getCitiesList = function(){
    return fetch('utils/city.list.json')
        .then(response => response.json());
}

export const getCountriesList = function(){
    return fetch('utils/countries.json')
        .then(response => response.json());
}

export const getWeather = function(cityID) {
    const api = new Api('https://api.openweathermap.org/');
    const relative = `data/2.5/forecast?id=${cityID}&APPID=${apiKey}`;
    return api.get(relative).then(response => response.json());
}