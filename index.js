import {
    getCitiesList,
    getCountriesList,
    getWeather
} from './api';

import { renderWeekdays } from 'elements/weekdays';
import { renderCountries } from 'elements/countries';
import { renderCities } from 'elements/cities';
import { renderWeathers } from 'elements/weathers';

window.cities = [];
window.weathers = [];

document.addEventListener('renderCountries', function(e){
    renderCountries(document.getElementById('countries'), e.detail);
});

document.addEventListener('renderCities', function(e){
    renderCities(document.getElementById('cities'), e.detail);
});

document.addEventListener('renderWeathers', function(e){
    renderWeathers(document.getElementById('weathers'), e.detail);
});

renderWeekdays(document.getElementById('days'));

// write App class and call these in App constructor
getCountriesList().then(countries => {
    const event = new CustomEvent('renderCountries', {detail: countries});
    document.dispatchEvent(event);

    countries.length &&
    getCitiesList().then(cities => {

        // this can be changed to store data anywhere else
        // this is just filtering
        window.cities = cities;

        //filter cities by default country
        const filteredCities = cities.filter(city => city.country === countries[0].code);

        const event = new CustomEvent('renderCities', {detail: filteredCities});
        document.dispatchEvent(event);

        cities.length && getWeather(cities[0].id).then(weathers => {
            window.weathers = weathers;

            const event = new CustomEvent('renderWeathers', {detail: weathers});
            document.dispatchEvent(event);
        })
    });
});

getWeather(616051).then(resp => {}); //console.log(resp, 'weatherList'));


